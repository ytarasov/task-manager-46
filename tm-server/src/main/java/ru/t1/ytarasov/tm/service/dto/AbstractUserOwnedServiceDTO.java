package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.dto.IUserOwnedServiceDTO;
import ru.t1.ytarasov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.ytarasov.tm.repository.dto.AbstractUserOwnedRepositoryDTO;

public abstract class AbstractUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO, R extends AbstractUserOwnedRepositoryDTO<M>>
        extends AbstractServiceDTO<M, R> implements IUserOwnedServiceDTO<M> {

    public AbstractUserOwnedServiceDTO(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

}
