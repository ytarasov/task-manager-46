package ru.t1.ytarasov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public final class TaskRepositoryDTO
        extends AbstractUserOwnedRepositoryDTO<TaskDTO> implements ITaskRepositoryDTO {

    public TaskRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final String jpql = String.format("FROM %s", getTableName());
        return entityManager
                .createQuery(jpql, TaskDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull String id) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p WHERE p.%s = :id",
                getTableName(), EntityConstantDTO.COLUMN_ID);
        return entityManager
                .createQuery(jpql, TaskDTO.class)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@NotNull Comparator comparator) {
        @NotNull final String jpql = String.format("SELECT p FROM %s m ORDER BY :sort", getTableName());
        return entityManager
                .createQuery(jpql, TaskDTO.class)
                .setParameter("sort", getSortedColumn(comparator))
                .getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@NotNull String userId, @NotNull Comparator comparator) {
        @NotNull final String jpql = String.format("SELECT p FROM %s m WHERE m.%s = :userId ORDER BY :sort",
                getTableName(), EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("sort", getSortedColumn(comparator))
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllTasksByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull final String jpql = String.format("SELECT t FROM %s t WHERE t.%s = :userId AND t.%s = :projectId",
                getTableName(), EntityConstantDTO.COLUMN_USER_ID, EntityConstantDTO.COLUMN_PROJECT_ID);
        return entityManager
                .createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@NotNull String userId) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p WHERE p.%s = :userId",
                getTableName(), EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable TaskDTO findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p WHERE p.%s = :id AND p.%s = :userId",
                getTableName(), EntityConstantDTO.COLUMN_ID, EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, TaskDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    protected @NotNull String getTableName() {
        return EntityConstantDTO.TABLE_TASK;
    }

}
