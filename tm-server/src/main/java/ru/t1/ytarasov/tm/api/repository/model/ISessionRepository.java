package ru.t1.ytarasov.tm.api.repository.model;

import ru.t1.ytarasov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
