package ru.t1.ytarasov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.DBConstant;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.t1.ytarasov.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO>
        extends AbstractRepositoryDTO<M> implements IUserOwnedRepositoryDTO<M> {

    public AbstractUserOwnedRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final String jpql = String.format("SELECT COUNT(m) FROM %s m WHERE m.%s = :userId",
                getTableName(), EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, int.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT COUNT(m) FROM %s m WHERE m.%s = :id AND m.%s = :userId",
                getTableName(), EntityConstantDTO.COLUMN_ID, EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void clear(@NotNull final String userId) {
        @Nullable final List<M> models = findAll(userId);
        if (models == null || models.isEmpty()) return;
        for (@NotNull final M model : models) remove(model);
    }

}
