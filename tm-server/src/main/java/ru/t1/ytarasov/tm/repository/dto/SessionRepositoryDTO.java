package ru.t1.ytarasov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.ISessionRepositoryDTO;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepositoryDTO extends AbstractUserOwnedRepositoryDTO<SessionDTO> implements ISessionRepositoryDTO {

    public SessionRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll() {
        @NotNull final String jpql = String.format("FROM %s", getTableName());
        return entityManager
                .createQuery(jpql, SessionDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull String id) {
        @NotNull final String jpql = String.format("SELECT s FROM %s s WHERE s.%s = :id",
                getTableName(), EntityConstantDTO.COLUMN_ID);
        return entityManager
                .createQuery(jpql, SessionDTO.class)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@NotNull String userId) {
        @NotNull final String jpql = String.format("SELECT s FROM %s s WHERE s.%s = :userId",
                getTableName(), EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = String.format("SELECT s FROM %s s WHERE s.%s = :id AND p.%s = :userId",
                getTableName(), EntityConstantDTO.COLUMN_ID, EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, SessionDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return EntityConstantDTO.TABLE_SESSION;
    }

}
