package ru.t1.ytarasov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.EntityConstant;
import ru.t1.ytarasov.tm.api.repository.model.ITaskRepository;
import ru.t1.ytarasov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository
        extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        @NotNull final String jpql = String.format("FROM %s", getTableName());
        return entityManager
                .createQuery(jpql, Task.class)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT t FROM %s t WHERE t.%s = :id",
                getTableName(), EntityConstant.COLUMN_ID);
        return entityManager
                .createQuery(jpql, Task.class)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final Comparator comparator) {
        @NotNull final String jpql = String.format("SELECT t FROM %s t ORDER BY :sort", getTableName());
        return entityManager
                .createQuery(jpql, Task.class)
                .setParameter("sort", getSortedType(comparator))
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final Comparator comparator) {
        @NotNull final String jpql = String.format("SELECT t FROM %s t WHERE t.%s = userId ORDER BY :sort",
                getTableName(), EntityConstant.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("sort", getSortedType(comparator))
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = String.format("SELECT t FROM %s t WHERE t.%s = :userId AND t.%s = :projectId",
                getTableName(), EntityConstant.COLUMN_USER_ID, EntityConstant.COLUMN_PROJECT_ID);
        return entityManager
                .createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final String jpql = String.format("SELECT t FROM %s t WHERE t.%s = userId",
                getTableName(), EntityConstant.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT t FROM %s t WHERE t.%s = :userId AND t.%s = id",
                getTableName(), EntityConstant.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return EntityConstant.TABLE_TASK;
    }

}
