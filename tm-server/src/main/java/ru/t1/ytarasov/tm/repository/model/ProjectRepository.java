package ru.t1.ytarasov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.EntityConstant;
import ru.t1.ytarasov.tm.api.repository.model.IProjectRepository;
import ru.t1.ytarasov.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository
        extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final Comparator comparator) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p ORDER BY :sort", getTableName());
        return entityManager
                .createQuery(jpql, Project.class)
                .setParameter("sort", getSortedType(comparator))
                .getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId, @NotNull final Comparator comparator) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p WHERE p.%s = :userId ORDER BY :sort",
                getTableName(), EntityConstant.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setParameter("sort", getSortedType(comparator))
                .getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        @NotNull final String jpql = String.format("FROM %s", getTableName());
        return entityManager
                .createQuery(jpql, Project.class)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT p FROM %s s WHERE p.%s = :id",
                getTableName(), EntityConstant.COLUMN_ID);
        return entityManager
                .createQuery(jpql, Project.class)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    public @Nullable List<Project> findAll(@NotNull final String userId) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p WHERE p.%s = :userId",
                getTableName(), EntityConstant.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT p FROM %s s WHERE p.%s = :id AND p.%s = :userId",
                getTableName(), EntityConstant.COLUMN_ID, EntityConstant.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, Project.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return EntityConstant.TABLE_PROJECT;
    }

}
