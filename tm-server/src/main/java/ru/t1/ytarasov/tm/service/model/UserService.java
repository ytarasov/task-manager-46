package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.model.IUserRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.model.IUserService;
import ru.t1.ytarasov.tm.exception.field.EmailEmptyException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.LoginEmptyException;
import ru.t1.ytarasov.tm.exception.field.PasswordEmptyException;
import ru.t1.ytarasov.tm.exception.user.UserNotFoundException;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.repository.model.UserRepository;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public final class UserService
        extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public List<User> findAll() throws Exception {
        @Nullable List<User> users;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            users = repository.findAll();
        } finally {
            entityManager.close();
        }
        return users;
    }

    @Override
    public int getSize() throws Exception {
        int size;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            size = repository.getSize();
        } finally {
            entityManager.close();
        }
        return size;
    }

    @NotNull
    @Override
    public User add(@Nullable final User model) throws Exception {
        if (model == null) throw new UserNotFoundException();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    public Collection<User> add(@Nullable Collection<User> models) throws Exception {
        if (models == null || models.isEmpty()) throw new UserNotFoundException();
        for (@NotNull final User model : models) add(model);
        return models;
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsById;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            existsById = repository.existsById(id);
        } finally {
            entityManager.close();
        }
        return existsById;
    }

    @Nullable
    @Override
    public User findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable User user;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            user = repository.findOneById(id);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public void clear() throws Exception {
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User remove(@Nullable User model) throws Exception {
        if (model == null) throw new UserNotFoundException();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    public User removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Override
    public void update(@Nullable final User model) throws Exception {
        if (model == null) throw new UserNotFoundException();
        model.setUpdated(new Date());
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            user = repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public @Nullable User findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User user;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            user = repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeByLogin(login);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public void lockUser(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUser(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

    @Override
    public void changePassword(@Nullable final String id, @Nullable final String newPassword) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (newPassword == null || newPassword.isEmpty()) throw new PasswordEmptyException();
        @Nullable final String newPasswordHash = HashUtil.salt(propertyService, newPassword);
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(newPasswordHash);
        update(user);
    }

}
